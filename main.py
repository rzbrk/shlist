#!/bin/env python3

import argparse
import lib.shlist as sl
import time
from yaml import load, Loader

def main(args):

    # Read config file
    f=open(args.config, 'r')
    config=load(f.read(), Loader=Loader)

    server=config['webdav']['server']
    user=config['webdav']['user']
    password=config['webdav']['password']
    remote_file=config['webdav']['remote_file']

    pin_printer=int(config['gpio']['pin_printer_enable'])
    pin_button=int(config['gpio']['pin_button'])

    # Create instance for shopping list
    l=sl.ShList(pin_printer=pin_printer, pin_button=pin_button)

    sandglass=("-", "\\", "|", "/")

    i=0
    print("Waiting for button ...", sandglass[i], end="")
    while(True):
        # Loop until button gpio goes low
        while(l.button_status()):
            time.sleep(0.2)
            i=i+1
            if (i>3):
                i=0
            print("\rWaiting for button ...", sandglass[i], end="")

        print("")

#       l.load_dummy_list(print_flag=True)
        l.load_list_from_webdav(
            server=server,
            user=user,
            password=password,
            remote_file=remote_file
            )
        
        l.printer_on()
        time.sleep(10)
        
        l.format_list()
        l.print_list()

        time.sleep(60)
        l.printer_off()

if __name__ == "__main__":
    """ This is executed when run from the command line """
    # Create parser
    parser = argparse.ArgumentParser()
 
    # Add arguments to the parser
    parser.add_argument("config")

    # Parse the arguments
    args = parser.parse_args()

    # Call main routine
    main(args)
