import re
import os
from webdav3.client import Client
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
from copy import copy
from datetime import datetime

class ShList:
    def __init__(self, pin_printer=None, pin_button=None):
        self.list=None
        self.list_isformatted=False
        self.pin_printer=pin_printer
        self.pin_button=pin_button
        self.setup_gpio()

    def setup_gpio(self):
        if not None==self.pin_printer:
            GPIO.setup(self.pin_printer, GPIO.OUT)
        if not None==self.pin_button:
            GPIO.setup(self.pin_button, GPIO.IN)

    def load_list_from_webdav(
        self,
        server,
        user,
        password,
        remote_file,
        ):
        options = {
            'webdav_hostname': "https://" + server
                + "/cloud/remote.php/dav/files/" + user + "/",
            'webdav_login': user,
            'webdav_password': password
        }
        client = Client(options)

        # Download the list to temporary local file
        local_file='/tmp/einkaufen.md'
        try:
            print("Load shopping list from webdav server ...")
            client.download_sync(remote_path=remote_file, local_path=local_file)
        except:
            # If remote file could not be downloaded, set self.list_raw to None
            print("  Error loading list")
            self.list = None
            self.list_isformatted = False
        else:
            # Update self.list with content from downloaded file
            print("  Success. Shopping list saved to ", local_file)
            self.list = local_file
            self.list_isformatted = False

    def format_list(self):
        print("Format raw list ...")
        if not None==self.list:
            # Read raw shopping list
            li = open(self.list, "r")
            c = li.readlines()
            li.close()

            # ToDo: Replace repeating \n with single \n

            # ## Shop --> \n========\n  Shp\n========
            for i in range(len(c)):
                c[i]=re.sub(r'^## (.*)', "\n"+"="*28+r"\n  \1\n"+"="*28, c[i])

            # ###### Category --> \nCategory\n--------
            for i in range(len(c)):
                c[i]=re.sub(r'^###### (.*)', r"\n\1\n"+"-"*28, c[i])

            # Remove all ticked items [x]
            for i in range(len(c)):
                if re.match(r'^\* \[x\]', c[i]):
                    c[i]=""

            # Replace markdown code for lists
            for i in range(len(c)):
                c[i]=re.sub(r'\* \[ \]', "\u2610", c[i])
                #c[i]=re.sub(r'\* \[x\]', "\u2611", c[i])

            # ToDo: Wrap too long lines (max 28 characters)

            # Output back to file
            lo = open(self.list, "w")
            # Add date/time
            now = datetime.now()
            lo.write(now.strftime('%Y-%m-%d, %H:%M:%S'))
            lo.write("\n")
            for i in range(len(c)):
                if (c[i]!=""):
                    lo.write(c[i])
            lo.close()

        else:
            self.list_formatted=None
            print("  Error: No shopping list loaded")

    def button_status(self):
        if not None==self.pin_button:
            return GPIO.input(self.pin_button)

    def printer_on(self):
        print("Turning printer on ...")
        if not None==self.pin_printer:
            GPIO.output(self.pin_printer, GPIO.HIGH)
            print("  Success")
        else:
            print("  Error")

    def printer_off(self):
        print("Turning printer off ...")
        if not None==self.pin_printer:
            GPIO.output(self.pin_printer, GPIO.LOW)
            print("  Success")
        else:
            print("  Error")

    def print_list(self):
        if not None==self.list:
            print("Send shopping list to system standard printer ...")
            cmd = ("/usr/bin/lpr %s" % self.list)
            os.system(cmd)
            print("  Done")

