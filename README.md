# Shopping List Printer

NOTE: This repository is work-in-progress and will be updated frequently in the
future. If you have any question not yet answered in the code or description
reach out to me.

## Description

This project is a device printing a shopping list. By pressing a button the
device will

1. fetch a shopping list file from a Nextcloud server,
2. activate a relay switch for powering a printer,
3. re-format the shopping list file to look nicer when printed out,
4. send the shopping list to the printer and finally 
5. deactivate the relay switch to power down the printer after printing.

The device consists of the following parts (hardware + software):

* A [Nextcloud installation](https://nextcloud.com/) with a markdown file
serving as a shopping list. The following snippet can serve as an example.
```
## Supermarket

###### Vegetables / fruits
* [ ] Bananas
* [ ] Apples
* [x] Grapes

###### Cooling
* [x] Milk
* [ ] Cheese

## Hardware store
* [ ] Hammer
* [x] Nails

```
The Nextcloud installation is on a separate server/computer. The device is just
accessing the Nextcloud server via the WebDAV protocol.
* A printer. In principle any printer works. I choosed an Epson thermal printer
that produces printouts just the right size to fit into a wallet or pocket. For
the Epson TM20II you can find a CUPS filter [here](https://github.com/rzbrk/epson-tm-t20-cups).
* A [Raspberry Pi Zero](https://www.raspberrypi.com/products/raspberry-pi-zero/).
Almost any computer running a modern Linux will work but I choosed the Zero because
of its small size and power consumption. Choose your favorite Linux operating system.
I usually go with [DietPi](https://dietpi.com/).
* A momentary switch and a corresponding 10kOhm pull up/down resistor.
* A relay switch

# Notes

## The shopping list
The shopping list is a shared file on the Nextcloud instance that can be
maintained by multiple user accounts e.g. via the Nextcloud mobile apps. The
check box items are very handy for supplies you buy frequently. If you again need
milk, simply uncheck the corresponding item in the shopping list rather than type
"milk" again.

I also created a separate user account for the shopping list printing device with
a limited quota and access only to the shopping list. For security reasons I did
not wanted to use the credentials of a "normal" user account on the device.

## Power supply and relay switch
The Epson printer comes with a 24V DC power supply. I choosed to modify the 24V
DC outlet into a "Y-cable" to both power the printer and the Raspberry Pi Zero.
That works fine for the small power consumption of the Raspberry Pi Zero,
especially because during boot time the printer is tuned off. To convert the 24V
to the 5V needed by the Raspberry Pi i used a
[LM2596 buck converter](https://www.amazon.com/LM2596/s?k=LM2596).

In the power rail going to the printer I inserted a relay switch that can be
controlled by the Raspberry Pi via a GPIO pin. The power switch on the printer
is always in the "on" position. But the printer is only supplied with power
when the shopping list is to be printed out by the relay switch. After printing
the printer is turned off again to save power.

If you wish you can skip the the relay completely and just power the Raspberry
Pi from a separate power adapter.

## Switch
For the switch to activate printing the shopping list I selected a
normally-closed switch because I had this lying around. You can also use a
normally-open switch but have to adapt the code. In a later version the selection
will be cnfigurable.

# Installation
* Prepare the Raspberry Pi with the operating system and ensure the printer is
working as expected.
* Install [pipenv](https://pipenv.pypa.io/en/latest/index.html).
* Clone this repository to the Raspberry Pi and change into the directory of
the repository (the directory with the file ``Pipfile``).
* Set-up the Python virtual environment with all dependencies:
```
pipenv install
```

# Configuration
Edit the file ``config.yml``. In the section webdav enter the server name and the
credentials for the Nextcloud server where the shopping list file resides on. In
the section gpio you should define the ports for the switch and the relay.

# Execution
To activate the script type:
```
pipenv run python3 main.py config.yml
```

This command can also be executed automatically after every boot of the Raspberry
Pi.

